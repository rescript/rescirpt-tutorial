
# ReScript初探

## 下载安装使用


ReScript 为主流平台(Windows/x64, Linux/x64, MacOS/x64/arm64)
提供了预先编译好的二进制文件供下载，如果用户只是想尝试下ReScript，也可以
直接体验提供的[在线编译器](https://rescript-lang.org/try)。

对于每个项目,用户可以使用sandbox模式选择不同的
版本的编译器.

```sh
git clone https://gitee.com/rescript/rescript-template
cd rescript-template
npm i # 安装
npx rescript build -w # 构建并处于监听模式
```

可以看到src文件夹里有两个文件 `Fib.res`, `Demo.res`.
ReScript的源代码的后缀名为 `.res`,`.resi`, 
`.res` 是源代码的主要实现, `.resi`是接口文件，
用户可选则要不要提供接口文件。

构建完成后对应生成的代码为 `Fib.mjs`, `Demo.mjs`

`Fib.res` 里面我们定义了一个fib函数

```re
let rec fib = (n)=>{
    switch n {
    | 0 | 1 => 1
    | n => fib(n-1) + fib(n-2)
    }
}
```

在另一个模块`Demo`里面我们写一个for循环来使用它:
```re
for i in 1 to 10 {
    Fib.fib(i)-> Js.log
}
```

运行输出结果为:
```sh
node src/Demo.mjs
tutorials/rescript-template%node src/Demo.mjs 
1
2
3
5
8
13
21
34
55
89
```


## ReScript第一个基本的特性：全局类型推断

我们可以使用命令

```sh
npx rescript dump src/Fib.cmi # 查询这个模块的信息
```
这个命令让rescript 构建Fib的二进制接口文件(`.cmi`)并且解析成可阅读的接口
它会输出
```re
let fib : int => int 
```

我们可以看到虽然我们没有写任何类型，编译器能够准确推断出 `src/Fib` 
这个模块里面导出了一个`fib`函数，它的类型是由编译器自己推导出来的。
英文名称叫type inference, 和typescript不同的是ReScript的类型推到
大部分情况下不需要标注函数的参数类型。

